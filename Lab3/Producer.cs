namespace cnu.andriyh.pop.lab3 {
    public class Producer
    {
        private StorageManager manager;
        private int id;
        private int itemsToProduce;

        public Producer(StorageManager manager, int id, int itemsToProduce)
        {
            this.manager = manager;
            this.id = id;
            this.itemsToProduce = itemsToProduce;
        }

        public void Run()
        {
            for (int i = 1; i <= itemsToProduce; i++)
            {
                manager.AddItem(id + "#" + i);
            }
        }
    }
}