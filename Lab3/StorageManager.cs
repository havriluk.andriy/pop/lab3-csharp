namespace cnu.andriyh.pop.lab3 {
#pragma warning disable CS8602
    public class StorageManager
    {
        private SemaphoreSlim empty;
        private SemaphoreSlim full;
        private SemaphoreSlim used;
        private LinkedList<string> items = new LinkedList<string>();

        public StorageManager(int maxSize)
        {
            empty = new SemaphoreSlim(0);
            full = new SemaphoreSlim(maxSize);
            used = new SemaphoreSlim(1);
        }

        public void AddItem(string item)
        {
            full.Wait();
            used.Wait();
            items.AddLast(item);
            Console.WriteLine("Додано новий предмет: " + item);
            empty.Release();
            used.Release();
        }

        public void GetItem(int consumer)
        {
            empty.Wait();
            used.Wait();
            string item = items.First.Value;
            items.RemoveFirst();
            Console.WriteLine("Споживач " + consumer + " вилучив предмет: " + item);
            full.Release();
            used.Release();
        }
    }
#pragma warning restore CS8602
}