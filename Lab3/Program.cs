﻿namespace cnu.andriyh.pop.lab3 {
    class Program
    {
        private static Random rng = new Random();
        public static void Main(string[] args)
        {
            int consumersCount = int.Parse(args[0]);
            int producersCount = int.Parse(args[1]);
            int maxSize = int.Parse(args[2]);
            int itemsCount = int.Parse(args[3]);

            StorageManager manager = new StorageManager(maxSize);
            List<Thread> allThreads = new List<Thread>(consumersCount + producersCount);
            for (int i = 1; i <= consumersCount; i++)
            {
                int itemsToConsume = itemsCount / consumersCount + ((i == consumersCount) ? itemsCount % consumersCount : 0);
                allThreads.Add(new Thread(new Consumer(manager, i, itemsToConsume).Run));
            }
            for (int i = 1; i <= producersCount; i++)
            {
                int itemsToProduce = itemsCount / producersCount + ((i == producersCount) ? itemsCount % producersCount : 0);
                allThreads.Add(new Thread(new Producer(manager, i, itemsToProduce).Run));
            }
            allThreads = allThreads.OrderBy(x => rng.Next()).ToList();
            foreach (var thread in allThreads)
            {
                thread.Start();
            }
        }
    }
}