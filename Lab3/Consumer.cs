namespace cnu.andriyh.pop.lab3 {
    public class Consumer
    {
        private StorageManager manager;
        private int id;
        private int itemsToConsume;

        public Consumer(StorageManager manager, int id, int itemsToConsume)
        {
            this.manager = manager;
            this.id = id;
            this.itemsToConsume = itemsToConsume;
        }

        public void Run()
        {
            for (int i = 1; i <= itemsToConsume; i++)
            {
                    manager.GetItem(id);
            }
        }
    }
}